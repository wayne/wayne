# Engineering director shadow onboarding - internal shadow

This issue template is used after an internal shadow is approved to participate in the program.

## Shadow onboarding

### Onboarding tasks for @wayne

* [ ] Mark the week in my Calendar indicating the shadow's name
* [ ] Add shadow to https://gitlab.com/wayne/wayne/-/project_members so they can edit this issue and send a welcome message
* [ ] Add shadow to `wayne_shadow_program` slack channel
* [ ] Add shadow to https://groups.google.com/a/gitlab.com/g/wayne-shadow-program/members so the shadow can access documents
* [ ] After the shadow schedules the intro meeting, document priorities in a Google doc and add to meeting/permissions for the shadow

/assign @wayne

### Onboarding Tasks for the shadow

* [ ] Schedule a meeting on the first and last day of the shadow program with Wayne 
* [ ] Confirm you can access Wayne's calendar. You are encouraged to attend all meetings that are not marked as private.
* [ ] Set your status in slack to indicate that you are shadowing (during your shadow week)
* [ ] Be prepared to write your notes in the shared Google doc

## Shadow offboarding

### Offboarding tasks for @wayne

* [ ] Update shadow schedule https://about.gitlab.com/handbook/engineering/development/shadow/director-shadow-program.html to move the shadow to the shadow alumni section
* [ ] Remove shadow from https://groups.google.com/a/gitlab.com/g/wayne-shadow-program/members

### Offboarding Tasks for the shadow

* [ ] Set your status in slack to indicate that you are no longer shadowing 
* [ ] Add links to all issues / merge requests you collaborated on to this issue
* [ ] Optional: Write a blog or a short social media post about the shadow program and review it with Wayne before posting. This helps to attract other shadows to the program.

