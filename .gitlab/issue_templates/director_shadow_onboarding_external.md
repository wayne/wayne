# Engineering director shadow onboarding - external shadow

This issue template is used after an external shadow is approved to participate in the program.

## External shadow onboarding

### Onboarding tasks for @wayne

* [ ] Update shadow schedule https://about.gitlab.com/handbook/engineering/development/shadow/director-shadow-program.html
* [ ] Mark the week in Calendar indicating the shadow's name
* [ ] Add shadow to https://gitlab.com/wayne/wayne/-/project_members so they can edit this issue
* [ ] Create IT ticket to add shadow to wayne_shadow_program slack channel (example issue to clone https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/23713)
* [ ] Invite shadow to meetings where confidential info will not be discussed. If there is a meeting where an external shadow has not been present in the past, ask the invitee if it is okay to have a shadow present.
* [ ] After the shadow is added to Slack, send a welcome message
* [ ] After the shadow schedules the intro meeting, document priorities in a Google doc and add to meeting/permissions for the shadow

/assign @wayne

### Onboarding Tasks for the shadow

* [ ] Schedule a meeting on the first and last day of the shadow program with Wayne using his calendly link
* [ ] Accept the slack invite
* [ ] Set your name and picture in Slack
* [ ] Set your name in Zoom if you haven't already done shadow
* [ ] Be prepared to write your notes in the shared Google doc
* [ ] Update this issue with your GitLab account name: `GitLab account name`

## External shadow offboarding

### Offboarding tasks for @wayne

* [ ] Update shadow schedule https://about.gitlab.com/handbook/engineering/development/shadow/director-shadow-program.html to move the shadow to the shadow alumni section

### Offboarding Tasks for the shadow

* [ ] Optional: Write a blog or a short social media post about the shadow program and review it with Wayne before posting. This helps to attract other shadows to the program.
* [ ] Add links to all issues / merge requests you collaborated on to this issue


